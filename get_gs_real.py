#!/usr/bin/env /home/mkolodru/bin/py

import scipy.sparse
import scipy.sparse.linalg
import sys
import numpy

num_gs=1
if len(sys.argv) > 1:
    num_gs=int(sys.argv[1])

basis_sz=int(open('H.out','r').readline())
Hvals=numpy.loadtxt('H.out',skiprows=1)

if basis_sz==1:
    open('gs.out','w').write('1\n')
    open('E_gs.out','w').write(str(Hvals[2])+'\n')
    sys.exit()

sz=Hvals.shape[0]
row_lst=numpy.zeros((sz),dtype=int)
col_lst=numpy.zeros((sz),dtype=int)
val_lst=numpy.zeros((sz),dtype=float)
row_lst[:]=Hvals[:,0]
col_lst[:]=Hvals[:,1]
val_lst[:]=Hvals[:,2]
H=scipy.sparse.csr_matrix((val_lst,(row_lst,col_lst)),shape=(basis_sz,basis_sz))
try:
    (E_gs,gs)=scipy.sparse.linalg.eigsh(H,k=num_gs,which='SA')
except Exception as error:
    sys.stderr.write('Issues with convergence...\n')
    sys.stderr.write(str(error)+'\n');
    sys.exit()

numpy.savetxt('gs.out',numpy.real(gs),fmt='%.25g')
numpy.savetxt('E_gs.out',E_gs,fmt='%.25g')
