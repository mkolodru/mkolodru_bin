#!/usr/bin/env python

import sys;
import string;

if len(sys.argv)<4:
    print "Proper usage: extract_column.py $in_file $out_file $column_#"
    assert 1==2;

infile=open(sys.argv[1],'r');
outfile=open(sys.argv[2],'w');
ind=int(sys.argv[3]);

for line in infile:
    spl=line.split();
    outfile.write(spl[ind]+'\n');

