#!/usr/bin/env /home/mkolodru/bin/py

import sys
import numpy
import numpy.linalg

basis_sz=int(open('H.out','r').readline())
Hvals=numpy.loadtxt('H.out',skiprows=1)
sz=Hvals.shape[0]
row_lst=numpy.zeros((sz),dtype=int)
col_lst=numpy.zeros((sz),dtype=int)
val_lst=numpy.zeros((sz))
row_lst[:]=Hvals[:,0]
col_lst[:]=Hvals[:,1]
val_lst[:]=Hvals[:,2]
H=numpy.zeros((basis_sz,basis_sz))
j=0
while j < len(row_lst):
    H[row_lst[j],col_lst[j]]=val_lst[j]
    j+=1

#Hamiltonian will be diagonalized by H_d = U^\dagger H U
(E,U)=numpy.linalg.eigh(H)

numpy.savetxt('E.out',E)
numpy.savetxt('U.out',numpy.real(U))
