#!/usr/bin/env py

import numpy
import subprocess
import sys

if len(sys.argv)<4:
	sys.stderr.write('Proper usage: multiply_col_by_value.py $col_name $value $newcol_name\n')
	sys.exit()

col_name=sys.argv[1]
versionmap=numpy.loadtxt('versionmap.dat',skiprows=1)
labels=open('versionmap.dat','r').readline().split()
col_ind=labels.index(col_name)
mult=float(sys.argv[2])

new_col=mult*versionmap[:,col_ind]
numpy.savetxt('temp_col.dat',new_col)

cmd='add_vmap_col.py temp_col.dat '+sys.argv[3]
print cmd
subprocess.call(cmd,shell=True)
