#!/usr/bin/env python

import sys

if len(sys.argv) < 2:
    print "Proper usage: print_nsteps.py $file"
    assert 1==2;

infile=open(sys.argv[1],'r');

cnt=0
for line in infile:
    cnt+=1

print cnt
