#!/usr/bin/env python

import numpy
import sys
import glob
import subprocess

if len(sys.argv) < 3:
	print 'Proper usage: plot_remaining.py $folder $variable'
	sys.exit()

folder=sys.argv[1]
var=sys.argv[2]

if folder[-1]!='/':
	folder+='/'

stats_sec=numpy.loadtxt('stats_section.dat',dtype=int)

for j in xrange(stats_sec.shape[0]):
	if stats_sec[j,1] == -1:
		folder_curr=folder+'b'+str(stats_sec[j,0])+'/'
		cmd='plotTraces.py '+str(folder_curr)+'/ '+var
		#	print cmd
		subprocess.call(cmd,shell=True)
