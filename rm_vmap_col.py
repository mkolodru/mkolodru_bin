#!/usr/bin/env python

import sys;
import numpy;

if len(sys.argv) < 2:
	print "Proper usage: rm_vmap_col.py $column_name";
	sys.exit()
	
linenum=0;

infile=open('versionmap.dat','r');
outfile=open('versionmap_backup.dat','w');
colName=sys.argv[1]
outval=''

for line in infile:
	outfile.write(line);
	pts=line.split()
	if linenum==0:
		try:
			colind=pts.index(colName)
		except:
			print 'Could not find a column labeled ' + str(colName)
			print pts
			sys.exit()
	lineout=''
	for j in range(len(pts)):
		if j != colind:
			lineout+=pts[j]+'\t'
	outval+=lineout.rstrip()+'\n'
	linenum+=1
	
outfile.close();
outfile=open('versionmap.dat','w');
outfile.write(outval);
