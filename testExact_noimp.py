#!/usr/bin/env python

import sys
import numpy
from pylab import *
import stats
import string

if len(sys.argv) < 4:
    print "Proper usage: testExact.py $eig_file $psiinit_file $type={1->sp,2->diag,3->both}"
    assert 1==2

eig_file=sys.argv[1]
basis_file=string.replace(eig_file,'eig','basis')
H_file=string.replace(eig_file,'eig','H')
psiinit_file=sys.argv[2]
psiavg_file=string.replace(psiinit_file,'psiinit','psiavg')
dt_file=string.replace(psiinit_file,'psiinit','dt')
psit_file=string.replace(psiinit_file,'psiinit','psitcoeff')

psiinit=numpy.loadtxt(psiinit_file);
psiavg=numpy.loadtxt(psiavg_file);
psitcoeff=numpy.loadtxt(psit_file);
eigcorr=numpy.loadtxt(eig_file,skiprows=1);
nExcList=0*eigcorr
psit=0*eigcorr
j=-1
for line in open(basis_file,'r'):
    if j!=-1:
        nExcList[j]=int(line.split()[0])
        psit[j]=psitcoeff[nExcList[j]]
    j+=1
full=numpy.loadtxt(eig_file)
S=float(full[0])
tvals=numpy.loadtxt(dt_file);
dt=tvals[1];
H=numpy.loadtxt(H_file,skiprows=1);
H_diag=0*H;
for j in range(numpy.shape(H)[0]):
    H_diag[j,j]=H[j,j];
H_offdiag=H-H_diag;
psiexp_offdiag=-dt*dot(H_offdiag,psiinit);
psiexp_diag=dt*dot(H_diag,psiinit)-S*dt*psiinit;
psieig=psiinit+psiexp_offdiag-psiexp_diag;
psidiff=psiinit-psiavg;

figure(1)
type=int(sys.argv[3])

if type==1:
    plot(psiavg);
    hold(True)
    plot(psiexp_offdiag,'r');
    figure(2)
    nonzero=[i for i in range(len(psiexp_offdiag)) if psiexp_offdiag[i]!=0]
    plot(psiavg[nonzero]-psiexp_offdiag[nonzero]);
    print stats.Stats(psiavg[nonzero]-psiexp_offdiag[nonzero])
elif type==2:
    plot(psidiff);
    hold(True)
    plot(psiexp_diag,'r');
    figure(2)
    plot(psidiff-psiexp_diag);
    print stats.Stats(psidiff-psiexp_diag)
else:
    psiavg=psiavg/sum(psiavg);
    psiinit=psiinit/sum(psiinit);
    plot(psiavg);
    hold(True)
    plot(psiinit,'r');
    figure(2);
    frac=psiavg/psiinit;
    diff=psiavg-psiinit
    plot(diff);
    print stats.Stats(diff)
    psiexp=eigcorr*psit
    psiexp=psiexp/sum(psiexp)
    figure(3)
    plot(psiavg)
    hold(True)
    plot(psiexp,'r')
    figure(4)
    diff=psiavg-psiexp
    frac=psiavg/psiexp;
    plot(diff)
    print stats.Stats(diff)

show()
