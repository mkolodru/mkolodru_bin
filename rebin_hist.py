#!/usr/bin/env python

import numpy;
import sys;

if len(sys.argv) < 4:
    print "Proper usage: rebin_hist.py $infile $outfile $bin_avg";
    assert(len(sys.argv) < 4);

#infile=open(sys.argv[1],'r');
oldhist=numpy.loadtxt(sys.argv[1]);
outfile=open(sys.argv[2],'w');
nbins=int(sys.argv[3]);

print numpy.shape(oldhist);

assert(numpy.shape(oldhist)[1]==2);

oldnumrows=numpy.shape(oldhist)[0];
newnumrows=int(numpy.floor(oldnumrows/nbins));

newhist=numpy.zeros((newnumrows,2));

for j in range(newnumrows):
    sum=0;
    for k in range(nbins):
        sum+=oldhist[j*nbins+k,1];
    newhist[j,0]=(oldhist[j*nbins,0]+oldhist[(j+1)*nbins-1,0])/2;
    newhist[j,1]=sum;
    outfile.write(str(newhist[j,0]) + '\t' + str(newhist[j,1]) + '\n')
