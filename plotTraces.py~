#!/usr/bin/env python

from pylab import *
import sys
import numpy
import glob
import os

if len(sys.argv) < 3:
	print 'Proper usage: plotTraces.py $file_1 $variable_2 ... $variable_N -[optional args]'

file1=sys.argv[1]

var_names=[]

if file1[-1]=='/': #Form 1, sys.argv[1] specifies just the folder
	folder=file1
else:
	lastind=file1.rfind('/')
	folder=''
	if lastind != -1:
		folder=file1[:(lastind+1)]
	var1=file1[(lastind+1):].split('_')[0]
	postfix=file1[(lastind+1+len(var1)):]
	var_names.append(var1)

j=2
opt_args=[]
while j < len(sys.argv):
	if sys.argv[j][0]=='-':
		opt_args.append(sys.argv[j][1:])
	else:
		var_names.append(sys.argv[j])
	j+=1

xstr='Step number'
ystr=None
posttitle=''
xrange=None
ylim_vals=None
slogx=False
slogy=False
xscale=1
xoffset=0

numskiprows=0

savefigs=False
savenum=0

for arg in opt_args:
	if arg[0]=='x':
		xstr=arg[2:]
	elif arg[0]=='y':
		ystr=arg[2:]
	elif arg[0]=='t':
		posttitle=arg[2:]
	elif arg[0]=='r':
		pts=arg[2:].split(',')
		if len(pts[0])==0:
			pts[0]='0'
		if len(pts[1])==0:
			pts[1]='-1'
		xrange=(int(pts[0]),int(pts[1]))
	elif arg[0]=='l':
		pts=arg[2:].split(',')
		ylim_vals=(float(pts[0]),float(pts[1]))
	elif arg[0]=='s':
		if arg[1] == 'x':
			slogx=True
		if arg[1] == 'y':
			slogy=True
	elif arg[0]=='c':
		xscale=float(arg[2:])
	elif arg[0]=='o':
		xoffset=float(arg[2:])
	elif arg[0]=='S':
		numskiprows=1
		
			
assert len(var_names) > 0
if file1[-1]=='/':
	base=folder+var_names[0]
	files=glob.glob(base+'.out')
	if len(files)==0:
		files=glob.glob(base+'_*.out')
	assert len(files) > 0
	postfix=files[0][len(base):]

length=-1

fignum=1
for var in var_names:
	filename=folder+var+postfix
	if os.path.getsize(filename) == 0:
		sys.stderr.write('Unable to continue, no data in file\n'+filename+'\n')
		break
	print 'Loading file '+filename
	if filename[-4:]=='.npy':
		data=numpy.load(filename,skiprows=numskiprows)
	else:
		data=numpy.loadtxt(filename,skiprows=numskiprows)
	if length == -1:
		if xrange==None:
			length=numpy.shape(data)[0]
			xvals=numpy.array(range(length))
			startval=0
		else:
			if xrange[1]==-1:
				xrange=(xrange[0],numpy.shape(data)[0]-1)
			length=xrange[1]-xrange[0]+1
			xvals=numpy.array(range(xrange[0],xrange[1]+1))
			startval=xrange[0]
		xvals=xvals*xscale+xoffset
	figure(fignum)
	if len(data.shape)==1:
		plot(xvals,data[startval:(startval+length)])
	elif len(data.shape)==2:
		#		yvals=numpy.arange(0,data.shape[1]-0.5)
		pcolor(data[startval:(startval+length-1),:-1].transpose())
	else:
		print 'Invalid data'
		sys.exit()
	if ylim_vals != None:
		ylim(ylim_vals)
	if slogx:
		semilogx()
	xlim(xscale*startval+xoffset,xscale*(startval+length)+xoffset)
	if slogy:
		semilogy()
	xlabel(xstr,fontsize='large')
	if ystr == None:
		ylabel(var,fontsize='large')
	else:
		ylabel(ystr,fontsize='large')
		
	if len(posttitle) > 0:
		title(posttitle,fontsize='x-large')
	else:
		title(var,fontsize='x-large')

	if savefigs:
		savefig(var+'_'+str(savenum)+'.png')
		
	fignum+=1

show()
