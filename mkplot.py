import numpy
import pylab

def mkplot(xind,ystr,fixedCols,fixedVals,myformat):
    versionmap=numpy.loadtxt("versionmap.dat",skiprows=1)
    ydata=numpy.loadtxt(ystr+"_stats.dat")
    assert len(fixedCols)==len(fixedVals);
    nfixed=len(fixedCols);
    #    print nfixed
    ind=[];
    #    print numpy.shape(versionmap)
    for i in range(numpy.shape(versionmap)[0]):
        j=0;
        while j < nfixed:
            if versionmap[i,fixedCols[j]]!=fixedVals[j]:
                break
            j+=1
        if j==nfixed:
            ind.append(i);
    #    print ind;
    ind12_versions=[int(i) for i in  versionmap[ind,0].copy()]
    #    print ind12_versions;
    
    #    print len(ydata[:,1]);
    ener_ind=[i for i in range(len(ydata[:,0])) if ydata[i,0] in ind12_versions];
    vmap_ind=[int(i) for i in list(ydata[ener_ind,0]-1)];
    
    #    print ener_ind
    #    print vmap_ind
    
    #    print versionmap[vmap_ind,xind]
    #    print ydata[ener_ind,1]
    #    print ydata[ener_ind,3]
    
    pylab.errorbar(versionmap[vmap_ind,xind],ydata[ener_ind,1],ydata[ener_ind,3],fmt=myformat)
    
def plotseries(xind, ystr, slowMatrix):
    infile=open("versionmap.dat",'r');
    firstline=infile.readline();
    ncols=len(firstline.split());
    infile.close();
    numslow=len(slowMatrix);
    assert numslow<=ncols-1
    slowSet=[]; temp=range(numslow);
    loopall(slowMatrix,slowSet,temp,0);
    #    print slowSet
    fixedCols=range(numslow);
    for j in range(numslow):
        fixedCols[j]=slowMatrix[j][0];
    #    print fixedCols;
    for fixedVals in slowSet:
        mkplot(xind,ystr,fixedCols,fixedVals,'');

def loopall(slowMatrix,slowSet,temp,ind):
    if ind==len(temp):
        slowSet.append(list(temp))
    else:
        for i in slowMatrix[ind][1]:
            temp[ind]=i;
            loopall(slowMatrix,slowSet,temp,ind+1);
    
