#!/usr/bin/env python

import numpy
import sys
import glob
import subprocess

def getv(f):
	return int(f.split('/')[-2][1:])

def foldercmp(f1,f2):
	return getv(f1)-getv(f2)

if len(sys.argv) < 3:
	print 'Proper usage: plot_all.py $folder $variable'
	sys.exit()

folder=sys.argv[1]
var=sys.argv[2]

if folder[-1]!='/':
	folder+='/'

folder_lst=sorted(glob.glob(folder+'v*/build'),cmp=foldercmp)

for j in xrange(len(folder_lst)):
	cmd='plotTraces.py '+str(folder_lst[j])+'/ '+var
	#	print cmd
	subprocess.call(cmd,shell=True)
