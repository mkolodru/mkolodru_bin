#!/usr/bin/env python

import glob
import sys
import subprocess

def filecmp(a,b):
	va=int(a[:-5].split('_')[-1])
	vb=int(b[:-5].split('_')[-1])
	return va-vb

fileList=glob.glob('*.qsub')
fileList=sorted(fileList,cmp=filecmp)

for file in fileList:
	cmd='qsub '+file
	subprocess.call(cmd,shell=True)
