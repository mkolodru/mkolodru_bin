#!/usr/bin/env py

import numpy
import pylab
import sys
import subprocess
import glob

def file_comp(f1,f2):
	n1=int(f1[:-3].split('_')[-1])
	n2=int(f2[:-3].split('_')[-1])
	return n1-n2

if len(sys.argv) < 6:
	print 'Proper usage: plot_range.py $tinit $tfinal $dt $Gfile $outbase [additional static plots]'
	sys.exit()

t1=float(sys.argv[1])
t2=float(sys.argv[2])
dt=float(sys.argv[3])
Gfile=sys.argv[4]
outbase=sys.argv[5]

slashind=Gfile.rfind('/')
if slashind==-1:
	folder='./'
else:
	folder=Gfile[:(slashind+1)]
tfile=folder+'t.out'

t=numpy.loadtxt(tfile)
G=numpy.loadtxt(Gfile)
xvals=numpy.arange(1,G.shape[1]+0.5)

assert len(t)==G.shape[0]

tout_vals=numpy.arange(t1,t2+dt/2,dt)

tout_ind=0
jlst=[]
ymin=0
ymax=0
for j in xrange(len(t)):
	if abs(t[j]-tout_vals[tout_ind]) < dt/2:
		jlst.append(j)
		Gmin=min(G[j,:])
		Gmax=max(G[j,:])
		if Gmin < ymin:
			ymin=Gmin
		if Gmax > ymax:
			ymax=Gmax
		tout_ind+=1
		if tout_ind >= len(tout_vals):
			break

ydiff=ymax-ymin
lim_max=ymax+ydiff*0.05
lim_min=ymin-ydiff*0.05

print 'Limits: ('+str(lim_min)+','+str(lim_max)+')'

Gadtl=[]
for j in xrange(6,len(sys.argv)):
	Gadtl.append(numpy.loadtxt(sys.argv[j]))

jind=0
while jind < len(jlst):
	j=jlst[jind]
	pylab.clf()
	pylab.plot(xvals,G[j,:],'.-',markersize=10,linewidth=1.5)
	for aind in xrange(len(Gadtl)):
		pylab.plot(xvals,Gadtl[aind],'.-',markersize=6,linewidth=1.5)
	pylab.xlabel('Distance')
	pylab.ylabel('Spin-spin correlation')
	pylab.title(r'$T_\mathrm{after}='+str(t[j])+r'$')
	pylab.ylim(lim_min,lim_max)
	pylab.savefig(outbase+'_'+str(jind)+'.ps')
	jind+=1

file_lst=sorted(glob.glob(outbase+'_*.ps'),cmp=file_comp)
cmd='convert '
for file in file_lst:
	cmd+=file+' '
cmd+=outbase+'.ps'
print cmd
subprocess.call(cmd,shell=True)

cmd='ps2pdf '+outbase+'.ps '+outbase+'.pdf'
print cmd
subprocess.call(cmd,shell=True)

cmd='rm *.ps'
print cmd
subprocess.call(cmd,shell=True)
