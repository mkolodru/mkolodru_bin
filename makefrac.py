#!/usr/bin/env py

import sys
import numpy

if len(sys.argv) < 3:
	print "Proper usage: makefrac.py $numerator_file $denom_file [$final_file]"
	assert 1==2

numerFile=sys.argv[1]
denomFile=sys.argv[2]
finalFile=numerFile.replace('N_','f_')
if len(sys.argv)>3:
	finalFile=sys.argv[3]

din=open(denomFile,'r')
out=open(finalFile,'w')
for nline in open(numerFile,'r'):
	dline=din.readline()
	frac=float(nline)/float(dline)
	out.write(str(frac)+'\n')
