#!/usr/bin/env python
import sys
import subprocess

if len(sys.argv) < 2:
    print 'Usage: mklatex.py $tex_file [-b]'
    sys.exit()

def exec_cmd(cmd):
    print cmd
    subprocess.call(cmd,shell=True)

tex_file=None
use_bib=False

for arg in sys.argv[1:]:
    if arg=='-b':
        use_bib=True
    elif tex_file==None:
        tex_file=arg+''

#latex_cmd='xelatex'
latex_cmd='pdflatex'

cmd1=latex_cmd+' '+tex_file
cmd2='bibtex '+tex_file.replace('.tex','.aux')
cmd3='evince '+tex_file.replace('.tex','.pdf')

exec_cmd(cmd1)
exec_cmd(cmd1)
if use_bib:
    exec_cmd(cmd2)
    exec_cmd(cmd1)
    exec_cmd(cmd1)
exec_cmd(cmd3)

