#!/usr/bin/env python

import numpy
import sys

data=numpy.loadtxt('versionmap.dat',skiprows=1,dtype=int)
val=-1
if len(sys.argv) > 1:
	val=int(sys.argv[1])
data[:,1]=val
numpy.savetxt('stats_section.dat',data[:,:2],fmt='%d')
