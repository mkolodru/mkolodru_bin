#!/usr/bin/env py

import numpy
import pylab
import sys

if len(sys.argv) < 2:
    print "Proper usage: plotHist.py $hist1_file $hist2_file ..."
    assert 1==2

for j in range(1,len(sys.argv)):
    data=numpy.loadtxt(sys.argv[j],skiprows=3)
    pylab.plot(data,label=sys.argv[j])
    print sys.argv[j]+': '+str(sum(data))
pylab.legend()
pylab.show()
