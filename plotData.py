#!/usr/bin/env python

import numpy
import pylab
import sys

if len(sys.argv) < 2:
	print 'Proper usage: plotData.py $file'
	sys.exit()
	
data=numpy.loadtxt(sys.argv[1],skiprows=1)
pylab.plot(data)
pylab.show()
