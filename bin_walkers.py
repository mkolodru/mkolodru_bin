#!/usr/bin/env python

import numpy;
import sys;

def getbin(x,numbins,deltax):
    val=int(numpy.floor(x/deltax+0.5));
    if val >= numbins:
        val=numbins-1;
    return val;

if(len(sys.argv)<5):
    print "Proper usage: bin_walkers.py $infile $outfile $nmax2 $numbins";
    assert 1==2;

infile=open(sys.argv[1],'r');
outfile=open(sys.argv[2],'w');
nmax2=float(sys.argv[3]);
nmax=numpy.sqrt(nmax2);
nbins=int(sys.argv[4]);
dx=nmax/nbins;

hist=numpy.zeros((nbins));

linenum=0;

for line in infile:
    if linenum < 2:
        linenum+=1;
        continue
    pts=line.split();
    nx=float(pts[4])
    ny=float(pts[5])
    nz=float(pts[6])
    n2=nx**2+ny**2+nz**2;
    n=numpy.sqrt(n2);
#    print str(n)+' '+str(nx)+' '+str(ny)+' '+str(nz)+' '+str(nbins)+' '+str(dx)+' '+str(getbin(n,nbins,dx));
    hist[getbin(n,nbins,dx)]+=1;
    linenum+=1;

center=dx/2;
for h in hist:
    outfile.write(str(center) + '\t' + str(h) + '\n');
    center+=dx;
