#!/usr/bin/env python

import numpy
import sys
import random
import subprocess

if len(sys.argv) < 3:
	print 'Proper usage: resize_walkers.py $walker_file $size'
	sys.exit()
	
walker_file=sys.argv[1]
backup_file=walker_file[:-4]+'_backup.out'
new_size=int(sys.argv[2])

linenum=0
out=open(backup_file,'w')
for line in open(walker_file,'r'):
	if linenum == 1:
		init_size=int(line)
	out.write(line)
	linenum+=1
out.close()

num_copies=int(numpy.floor(new_size/init_size))
to_rgen=new_size%init_size
print 'num_copies='+str(num_copies)
print 'to_rgen='+str(to_rgen)
rgen_list=random.sample(xrange(init_size),to_rgen)
rgen_list.sort()

out=open(walker_file,'w')
linenum=-2
k=0
for line in open(backup_file,'r'):
	if linenum == -2:
		out.write(line)
	elif linenum == -1:
		out.write(str(new_size)+'\n')
	else:
		for j in xrange(num_copies):
			out.write(line)
		if k < to_rgen and rgen_list[k] == linenum:
			out.write(line)
			k+=1
	linenum+=1
out.close()

cmd='rm '+backup_file
print cmd

subprocess.call(cmd,shell=True)
