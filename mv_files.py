#!/usr/bin/env python

import shutil;
import sys;

if len(sys.argv)<4:
    print "Proper usage: mv_files $src_base $dest_base $number_file";
    assert 1==2;

assert sys.argv[1][-1]=='/';
assert sys.argv[2][-1]=='/';

src_base=sys.argv[1]+'v';
dest_base=sys.argv[2]+'v';

for line in open(sys.argv[3],'r'):
    shutil.move(src_base+line[:-1],dest_base+line[:-1]);
