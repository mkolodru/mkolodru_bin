#! /usr/bin/env python

import stats;
import numpy;
import sys;

data=numpy.loadtxt(sys.argv[1]);
if len(sys.argv)==3:
    print stats.Stats(data[int(sys.argv[2]):]);
elif len(sys.argv)==4:
    print stats.Stats(data[int(sys.argv[2]):int(sys.argv[3])]);
else:
    print stats.Stats(data);
