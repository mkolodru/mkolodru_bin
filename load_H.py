import numpy

def load_H(H_file):
	infile=open(H_file,'r')
	j=-1
	for line in infile:
		if j < 0:
			NCONFIG=int(line)
			H=numpy.zeros((NCONFIG,NCONFIG))
		else:
			k=0
			for pt in line.split():
				H[j,k]=float(pt)
				k+=1
		j+=1
	return H
