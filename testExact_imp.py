#!/usr/bin/env python

import sys
import numpy
from pylab import *
import stats
import string
import load_H

def smart_divide(a,b):
	c=a/b
	for j in xrange(len(b)):
		if b[j]==0:
			#			assert a[j]==0
			c[j]=0
	return c

if len(sys.argv) < 4:
	print "Proper usage: testExact.py $eig_file $psiinit_file $type={1->sp,2->diag,3->both}"
	assert 1==2

eig_file=sys.argv[1]
basis_file=string.replace(eig_file,'eig','basis')
H_file=string.replace(eig_file,'eig','H')
psiinit_file=sys.argv[2]
psifinal_file=string.replace(psiinit_file,'psiinit','psifinal')
dt_file=string.replace(psiinit_file,'psiinit','dt')
S_file=string.replace(psiinit_file,'psiinit','S')
psit_file=string.replace(psiinit_file,'psiinit','psit')
eigcorr_file=string.replace(psiinit_file,'psiinit','eigXpsit')

psit=numpy.loadtxt(psit_file)
eigcorr=numpy.loadtxt(eigcorr_file)
eigcorr=eigcorr/sum(eigcorr)
eigcorr=smart_divide(eigcorr,psit)

# Get the wavefunctions
# psiinit and psifinal are from walker histograms
# I will divide out psit from each so that they reflect the
# actual wavefunction
psiinit=smart_divide(numpy.loadtxt(psiinit_file),psit)
psifinal=smart_divide(numpy.loadtxt(psifinal_file),psit)

S=float(numpy.loadtxt(S_file)[0])
E0=float(open(eig_file,'r').readline())
dt=numpy.loadtxt(dt_file)[0]

type=int(sys.argv[3])


if type != 3:
	print 'About to load H'
	#	H=numpy.loadtxt(H_file,skiprows=1)
	H=load_H.load_H(H_file)
	print 'Loaded H'
	H_diag=0*H
	for j in range(numpy.shape(H)[0]):
		H_diag[j,j]=H[j,j]
	H_offdiag=H-H_diag
    #Based on psiinit, figure out the expected diag and off-diag wfns after one step
	psiexp_offdiag=-dt*dot(H_offdiag,psiinit)
	psiexp_diag=dt*dot(H_diag,psiinit)-S*dt*psiinit
	psiexp_offdiag=psiexp_offdiag*psit
	psiexp_offdiag=smart_divide(psiexp_offdiag,psit)
	#	psiexp_diag=psiexp_diag*psit
	#	psiexp_diag=smart_divide(psiexp_diag,psit)
	psidiff=psiinit-psifinal

figure(1)

if type==1:
	plot(psifinal,label='psifinal')
	hold(True)
	plot(psiexp_offdiag,'r',label='psiexp_offdiag')
	legend(loc='best')
	figure(2)
	nonzero=[i for i in range(len(psiexp_offdiag)) if psiexp_offdiag[i]!=0]
	plot(psifinal[nonzero]-psiexp_offdiag[nonzero])
	print stats.Stats(psifinal[nonzero]-psiexp_offdiag[nonzero])
elif type==2:
	plot(psidiff,label='psiinit-psifinal')
	hold(True)
	plot(psiexp_diag,'r',label='psiexp_diag')
	legend(loc='best')
	figure(2)
	plot(psidiff-psiexp_diag)
	print stats.Stats(psidiff-psiexp_diag)
else:
	psifinal_exp=(1-dt*(E0-S))*psiinit
	plot(psifinal,label='psifinal')
	#	plot(eigcorr,label='eigcorr')
	hold(True)
	plot(psifinal_exp,'r',label='psiinit')
	legend(loc='best')
	figure(2)
	#	diff=eigcorr-psiinit
	diff=psifinal-psifinal_exp
	plot(diff)
	print stats.Stats(diff)

show()
