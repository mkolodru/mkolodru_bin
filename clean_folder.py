#!/usr/bin/env python

#This script will clean up all the unimportant data
#in a run directory, namely
#  1) Backup files, *~
#  2) qsub files, *.qsub*
#  3) in files, *.in
#  4) err files, *.err
#  5) out files, *.out
#  6) Non-python executables
#  7) Log files, *.log
#  8) Image files, *.pdf, *png, and *.ps
#I will assume that anything desired to be saved has been moved
#to subdirectories.  Also assuming that print_logs.py
#has already been run.

import subprocess
import sys
import os
import glob

if len(sys.argv) < 2:
	print 'Proper usage: clean_folder.py $folder'
	sys.exit()

folder=sys.argv[1]
if folder[-1]!='/':
	folder+='/'

cmd='rm '+folder+'*~'
print cmd
subprocess.call(cmd,shell=True)
cmd='rm '+folder+'*.qsub*'
print cmd
subprocess.call(cmd,shell=True)
cmd='rm '+folder+'*.in'
print cmd
subprocess.call(cmd,shell=True)
cmd='rm '+folder+'*.err'
print cmd
subprocess.call(cmd,shell=True)
cmd='rm '+folder+'*.out'
print cmd
subprocess.call(cmd,shell=True)
cmd='rm '+folder+'*.log'
print cmd
subprocess.call(cmd,shell=True)
cmd='rm '+folder+'*.pdf'
print cmd
subprocess.call(cmd,shell=True)
cmd='rm '+folder+'*.png'
print cmd
subprocess.call(cmd,shell=True)
cmd='rm '+folder+'*.ps'
print cmd
subprocess.call(cmd,shell=True)

all_files=glob.glob(folder+'*')
folder_files=glob.glob(folder+'*/')
for j in xrange(len(folder_files)):
	folder_files[j]=folder_files[j][:-1]
exec_files=''
for file in all_files:
	if file.find('.')==-1 and (file not in folder_files):
		exec_files+=file+' '
cmd='rm '+exec_files
print cmd
subprocess.call(cmd,shell=True)



