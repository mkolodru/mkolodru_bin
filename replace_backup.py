#!/usr/bin/env py

import numpy
import sys
import os
import glob
import subprocess

temp_lst=glob.glob('v*/build/tempBackup_*.out')

for temp_file in temp_lst:
	walker_file=temp_file.replace('tempBackup','walkers')
	tsz=0
	wsz=0
	for line in open(temp_file,'r'):
		tsz+=1
	for line in open(walker_file,'r'):
		wsz+=1

	if tsz > wsz:
		cmd='mv '+str(temp_file)+' '+str(walker_file)
		print cmd
		subprocess.call(cmd,shell=True)
	else:
		cmd='rm '+str(temp_file)
		print cmd
		subprocess.call(cmd,shell=True)
		
		
