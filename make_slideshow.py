#!/usr/bin/env py

import numpy
import subprocess
import glob
import sys

def file_cmp(f1,f2):
	v1=float(f1[:-4].split('_')[-1])
	v2=float(f2[:-4].split('_')[-1])
	if v1<v2:
		return -1
	return 1

if len(sys.argv) < 2:
	print 'Proper usage: make_slideshow.py $example_file [-r=False]'
	sys.exit()

file_base=sys.argv[1]
remove_after=False
if len(sys.argv) > 2 and sys.argv[2][:2]=='-r':
	remove_after=True
index=file_base.rfind('_')
assert index!=-1
file_base=file_base[:index]
print 'file_base='+str(file_base)
files=sorted(glob.glob(file_base+'_*.png'),cmp=file_cmp)

cmd='convert '
for file in files:
	cmd+=file+' '
cmd+=file_base+'_slideshow.ps'
print cmd
subprocess.call(cmd,shell=True)

cmd='ps2pdf '+file_base+'_slideshow.ps '+file_base+'_slideshow.pdf'
print cmd
subprocess.call(cmd,shell=True)

cmd='rm '+file_base+'_slideshow.ps'
if remove_after:
	cmd+='; rm '+file_base+'_*.png'
print cmd
subprocess.call(cmd,shell=True)
