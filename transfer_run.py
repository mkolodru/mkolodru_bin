#!/usr/bin/env python

# Transfer a computational run to another computer
# Program assumes that we are in directory
# $HOME/$PROJECT/build/

import sys
import os
import glob
import subprocess

def run_cmd(cmd):
    print cmd
    subprocess.call(cmd,shell=True)

def is_empty(file):
    cnt=0
    for line in open(file,'r'):
        cnt+=1
    return cnt==0

if len(sys.argv) < 3:
    print 'Usage: transfer_run.py $run_name $transfer_to [-d/r]'
    sys.exit()

run_name=sys.argv[1]
transfer_to=sys.argv[2]
if run_name[-1]=='/':
    run_name=run_name[:-1]

transfer_run=True
transfer_data=True
if len(sys.argv) > 3:
    if sys.argv[3]=='-d':
        transfer_run=False
        print 'Only transferring the data'
    elif sys.argv[3]=='-r':
        transfer_data=False
        print 'Only transferring the run'

info_file='/home/mkolodru/info/cpu_info.dat'
ip_remote=None
run_folder=None
data_folder=None

for line in open(info_file,'r'):
    pts=line.split()
    if transfer_to in pts[:-2]:
        ip_remote=pts[0]
        run_folder=pts[-2]
        data_folder=pts[-1]
        break

if ip_remote==None:
    print 'Unable to locate computer named '+transfer_to+' in '+info_file
    sys.exit()

data_local_root=open('/home/mkolodru/info/data_local.dat','r').readline()[:-1]
if data_local_root[-1]!='/':
    data_local_root+='/'

if run_folder[-1]!='/':
    run_folder+='/'
if data_folder[-1]!='/':
    data_folder+='/'

curr_dir=os.getcwd()+'/'
pts=curr_dir[:-1].split('/')
if pts[-1]!='build':
    print 'You are not in the correct directory to call this command'
    sys.exit()
project_name=pts[-2]

if transfer_run:
    run_local=curr_dir+run_name+'/'
    if not os.path.exists(run_local):
        print 'Unable to find directory: '+run_local
        sys.exit()
    tgz_local=run_local[:-1]+'.tgz'

    build_remote=run_folder+project_name+'/build/'
    run_remote=build_remote+run_name+'/'
    tgz_remote=run_remote[:-1]+'.tgz'
    run_cmd('ssh '+ip_remote+' mkdir -p '+build_remote)

    # Create the tar file
    run_cmd('tar -czf '+tgz_local+' -C '+curr_dir+' '+run_name)

    # Transfer it to the remote computer
    run_cmd('scp '+tgz_local+' '+ip_remote+':'+build_remote+' 2> temp.temp')
    if os.path.exists('temp.temp') and not is_empty('temp.temp'):
        print 'There was some sort of error in transferring the file:'
        for line in open('temp.temp'):
            print line
        sys.exit()

    # Unzip it remotely
    run_cmd('ssh '+ip_remote+' tar -xzf '+tgz_remote+' -C '+build_remote+' 2> temp.temp')
    if os.path.exists('temp.temp') and not is_empty('temp.temp'):
        print 'There was some sort of error in unzipping the file:'
        for line in open('temp.temp'):
            print line
        sys.exit()

    # Remove the tar file locally
    if os.path.exists(tgz_local):
        os.remove(tgz_local)

    # Remove the tar file remotely
    run_cmd('ssh '+ip_remote+' rm '+tgz_remote)

if transfer_data:
    data_local=data_local_root+run_name+'/'
    if not os.path.exists(data_local):
        print 'Unable to find directory: '+data_local
        sys.exit()
    tgz_local=data_local[:-1]+'.tgz'

    data_remote=data_folder+run_name+'/'
    tgz_remote=data_remote[:-1]+'.tgz'
    run_cmd('ssh '+ip_remote+' mkdir -p '+data_folder)

    # Create the tar file
    run_cmd('tar -czf '+tgz_local+' -C '+data_local_root+' '+run_name)

    # Transfer it to the remote computer
    run_cmd('scp '+tgz_local+' '+ip_remote+':'+data_folder+' 2> temp.temp')
    if os.path.exists('temp.temp') and not is_empty('temp.temp'):
        print 'There was some sort of error in transferring the file:'
        for line in open('temp.temp'):
            print line
        sys.exit()

    # Unzip it remotely
    run_cmd('ssh '+ip_remote+' tar -xzf '+tgz_remote+' -C '+data_folder+' 2> temp.temp')
    if os.path.exists('temp.temp') and not is_empty('temp.temp'):
        print 'There was some sort of error in unzipping the file:'
        for line in open('temp.temp'):
            print line
        sys.exit()

    # Remove the tar file locally
    if os.path.exists(tgz_local):
        os.remove(tgz_local)

    # Remove the data locally if nothing has failed yet (too big to duplicate)
    if os.path.exists(data_local):
        run_cmd('rm -r '+data_local)

    # Remove the tar file remotely
    run_cmd('ssh '+ip_remote+' rm '+tgz_remote)

# Remove temporary files
if os.path.exists('temp.temp'):
    os.remove('temp.temp')
