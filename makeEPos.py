#!/usr/bin/env py

import glob
import string
import sys

folder='./'
if len(sys.argv) > 1:
	folder=sys.argv[1]
if folder[-1]!='/':
	folder+='/'

files=glob.glob(folder+"b*/Enum.out");
for f in files:
    print f
    infile2=open(string.replace(f,'num','den'),'r');
    outfile1=open(string.replace(f,'num','numpos'),'w');
    outfile2=open(string.replace(f,'num','denpos'),'w');
    for line in open(f,'r'):
        line2=infile2.readline();
        d1=float(line);
        d2=float(line2);
        if d2 < 0:
            d1*=-1;
            d2*=-1;
        s1='%.17f' % d1
        s2='%.17f' % d2
        outfile1.write(s1+'\n')
        outfile2.write(s2+'\n')
                     
        
