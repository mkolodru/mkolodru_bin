#!/usr/bin/env python

import numpy
import sys
import stats

def match(v1,v2,vmap,cols):
	row=0
	r1=-1
	r2=-1
	while row<vmap.shape[0]:
		if v1==int(vmap[row,0]):
			r1=row
		if v2==int(vmap[row,0]):
			r2=row
		row+=1
	if r1<0 or r2<0:
		print 'Could not find v1='+str(v1)+' or v2='+str(v2)
		sys.exit()

	for col in cols:
		if abs(vmap[r1,col]-vmap[r2,col])>1e-9:
			return False
	return True

if len(sys.argv) < 4:
	print 'Proper usage: combine_stats.py $infile $outfile [$match_cols]'
	sys.exit()

orig_stats=numpy.loadtxt(sys.argv[1])
new_stats=orig_stats*0

vmap=numpy.loadtxt('versionmap.dat',skiprows=1)
labels=open('versionmap.dat').readline().split()
vnum_col=labels.index('vnum')

match_cols=[]
weighted=False
for j in xrange(3,len(sys.argv)):
	if sys.argv[j]=='-w':
		weighted=True
		print 'Weighted not yet implemented'
	match_cols.append(labels.index(sys.argv[j]))

used=numpy.zeros((orig_stats.shape[0]),dtype=int)

row_init=0
row_new=0
while row_init < orig_stats.shape[0]:
	if used[row_init]==0:
		vnum_init=int(orig_stats[row_init,0])
		row=row_init
		mean_lst=[]
		std_lst=[]
		while row < orig_stats.shape[0]:
			vnum=int(orig_stats[row,0])
			if match(vnum,vnum_init,vmap,match_cols):
				mean_lst.append(orig_stats[row,1])
				std_lst.append(orig_stats[row,3])
				used[row]=1
			row+=1
		(m,v,s,c)=stats.Stats(numpy.array(mean_lst))
		if len(mean_lst)==1:
			m=mean_lst[0]
			s=std_lst[0]
		new_stats[row_new,0]=vnum_init
		new_stats[row_new,1]=m
		new_stats[row_new,2]=v
		new_stats[row_new,3]=s
		new_stats[row_new,4]=c
		row_new+=1
	row_init+=1

numpy.savetxt(sys.argv[2],new_stats[:row_new,:],fmt='%d\t%.19f\t%.19f\t%.19f\t%.19f')
