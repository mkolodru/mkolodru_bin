#!/usr/bin/env python

import sys
import numpy

if len(sys.argv) < 2:
	print 'Proper usage: makecolfile.py $file'
	sys.exit()

outstr=''

for line in open(sys.argv[1],'r'):
	pts=line.split()
	for j in xrange(len(pts)):
		if j==0:
			outstr+=str(int(numpy.round(float(pts[0]))))+'\t'
		else:
			outstr+=pts[j]+'\t'
	outstr+='\n'

open(sys.argv[1],'w').write(outstr)
			
