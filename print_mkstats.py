#! /usr/bin/env python

import stats;
import numpy;
import sys;

data=numpy.loadtxt(sys.argv[1]);
col=int(sys.argv[2]);
print stats.mkstats(data[:,col]);
