#!/usr/bin/env py

import numpy
import subprocess
import sys

if len(sys.argv)<4:
	sys.stderr.write('Proper usage: multiply_cols.py $col_1 $col_2 $newcol_name\n')
	sys.exit()

col_1=sys.argv[1]
col_2=sys.argv[2]
versionmap=numpy.loadtxt('versionmap.dat',skiprows=1)
labels=open('versionmap.dat','r').readline().split()
col_1_ind=labels.index(col_1)
col_2_ind=labels.index(col_2)
assert col_1_ind != -1
assert col_2_ind != -1

new_col=versionmap[:,col_1_ind]*versionmap[:,col_2_ind]
numpy.savetxt('temp_col.dat',new_col)

cmd='add_vmap_col.py temp_col.dat '+sys.argv[3]
print cmd
subprocess.call(cmd,shell=True)
