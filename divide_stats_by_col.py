#!/usr/bin/env python

import numpy
import sys

if len(sys.argv) < 4:
	print 'Proper usage: divide_stats_by_col.py $stats_in $col_name $stats_out'
	sys.exit()

data=numpy.loadtxt(sys.argv[1])
vmap=numpy.loadtxt('versionmap.dat',skiprows=1)
labels=open('versionmap.dat','r').readline().split()
col_ind=labels.index(sys.argv[2])
out=open(sys.argv[3],'w')

for j in xrange(numpy.shape(data)[0]):
	vmaprow=int(numpy.round(data[j,0]))-1
	assert abs(data[j,0]-vmap[vmaprow,0]) < .01
	data[j,1]/=vmap[vmaprow,col_ind]
	data[j,3]/=vmap[vmaprow,col_ind]
	out.write(str(vmaprow+1))
	for k in xrange(1,5):
		out.write('\t'+str(data[j,k]))
	out.write('\n')
