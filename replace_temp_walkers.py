#!/usr/bin/env python

import sys
import glob
import os.path
import subprocess

if len(sys.argv) < 2:
	print 'Proper usage: replace_temp_walkers.py $folder'
	sys.exit()

folder=sys.argv[1]
if folder[-1]!='/':
	folder+='/'

temp_files=glob.glob(folder+'b*/tempBackup.out')
for temp_file in temp_files:
	walker_file=temp_file.replace('tempBackup','walkers')
	if os.path.getsize(temp_file)>os.path.getsize(walker_file):
		cmd='mv '+temp_file+' '+walker_file
		print cmd
		subprocess.call(cmd,shell=True)
	else:
		cmd='rm '+temp_file
		print cmd
		subprocess.call(cmd,shell=True)
