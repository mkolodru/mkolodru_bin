#!/usr/bin/env python

import numpy
import sys

if len(sys.argv) < 4:
	print 'Proper usage: add_stats.py $in1 $in2 $out'
	sys.exit()

data1=numpy.loadtxt(sys.argv[1])
data2=numpy.loadtxt(sys.argv[2])

sz=numpy.shape(data1)[0]
assert sz==numpy.shape(data2)[0]

out=open(sys.argv[3],'w')

sum=data1[:,1]+data2[:,1]
std=numpy.sqrt(data1[:,3]**2+data2[:,3]**2)

for j in xrange(sz):
	out.write(str(int(round(data1[j,0])))+'\t'+str(sum[j])+'\t0\t'+str(std[j])+'\t0\n')
