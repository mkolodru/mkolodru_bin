#!/usr/bin/env python

import subprocess
import sys
import os
import glob

if len(sys.argv) < 1:
	print 'Proper usage: retrieve_data.py [$folder]'
	sys.exit()

rel_folder=''
if len(sys.argv) > 1:
	rel_folder=sys.argv[1]
if len(rel_folder)>0 and rel_folder[-1]=='/':
	rel_folder=rel_folder[:-1]

curr_folder=os.getcwd()

if len(rel_folder)==0:
	folder=curr_folder
elif rel_folder[0]!='/':
	folder=curr_folder+'/'+rel_folder
else:
	folder=rel_folder

pts=folder.split('/')
assert len(pts) >= 4
run_name=pts[-1]
assert pts[-2]=='build'
sim_name=pts[-3]
assert pts[-4]=='mkolodru'

tar_postfix=run_name+'_data.tgz'
tar_file=folder+'/'+tar_postfix

#First scp the tar file from the external h.d.
ext_hd_dir='mkolodru-cmt:~/backup/'
ext_folder=ext_hd_dir+sim_name+'/build/'
cmd='scp '+ext_folder+tar_postfix+' '+folder+'/'
print cmd
subprocess.call(cmd,shell=True)

#Then extract the tar file and remove it
cmd='cd '+folder+'; tar -xzf '+tar_postfix+'; rm '+tar_postfix+'; cd -'
print cmd
subprocess.call(cmd,shell=True)

#Finally move the folders from b*/ to r*/ to let me know they can be deleted
bfolder_lst=glob.glob(folder+'/b*/')
for bfolder in bfolder_lst:
	rfolder=folder+'/'+bfolder.split('/')[-2].replace('b','R')
	cmd='mv '+bfolder+' '+rfolder
	print cmd
	subprocess.call(cmd,shell=True)
	
