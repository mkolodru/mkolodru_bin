#!/usr/bin/env python

import sys
import numpy
import pylab

pylab.figure(1)

datalist=[]

for j in range(1,len(sys.argv)):
	sr=0
	filestr=sys.argv[j]
	colonind=filestr.find(':')
	if colonind!=-1:
		sr=int(filestr[(colonind+1):])
		filestr=filestr[:colonind]
	data=numpy.loadtxt(filestr,skiprows=sr)
	#	if numpy.shape(data)[0]==1:
	#		data=numpy.loadtxt(sys.argv[j],skiprows=1)
	pylab.plot(data,label='Trace #'+str(j))
	datalist.append(data)

pylab.legend()

if len(sys.argv) == 3 and len(datalist[1])==len(datalist[0]):
	pylab.figure(2)
	diff=datalist[1]-datalist[0]
	pylab.plot(diff)
	print 'Min diff='+str(min(diff))
	print 'Max diff='+str(max(diff))

pylab.show()
