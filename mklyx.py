#!/usr/bin/env python

import sys
import subprocess

def call(cmd):
    print cmd
    subprocess.call(cmd,shell=True)

if len(sys.argv) < 2:
    print 'Usage: compile_lyx.py $file [optional mklatex commands]'
    sys.exit()

lyx_file=sys.argv[1]
arg_str=''
for arg in sys.argv[2:]:
    arg_str+=' '+arg
call('lyx --force-overwrite --export pdflatex '+lyx_file)
call('mklatex.py '+lyx_file.replace('.lyx','.tex')+arg_str)
