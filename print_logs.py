#!/usr/bin/env python

import sys
import glob
import subprocess

#Takes a string of form "run#_DATE" where DATE is MMDDYY
#and returns a tuple (YY,MM,DD,#)
def getvals(x):
    pts=x.split('.')[0].split('_');
    nn=int(pts[0][3:])
    mm=int(pts[1][0:2])
    dd=int(pts[1][2:4])
    yy=int(pts[1][4:6])
    return (yy,mm,dd,nn)
    

#Comparison operator for strings of form "run#_DATE/*.log"
#Sorts first by date then by run number
def run_compare(x, y):
    xv=getvals(x.split('/')[1])
    yv=getvals(y.split('/')[1])
    for j in range(4):
        if xv[j]-yv[j] != 0:
            return xv[j]-yv[j]
    return 0

outfile='logs.log'
if len(sys.argv) > 1:
	outfile=sys.argv[1]
print outfile

cmd='mkdir -p log_files'
sys.stderr.write(cmd+'\n')
subprocess.call(cmd,shell=True)

lst_orig=glob.glob('run*/run*.log')
cmd='cp '
for f in lst_orig:
    if f.split('/')[-1].count('.')==1: # Only legitimate if single '.', i.e. run_***.log not run_***.873487837.0.log
        cmd+=f+' '
        
if cmd!='cp ':
    cmd+='log_files/'
    sys.stderr.write(cmd+'\n')
    subprocess.call(cmd,shell=True)

filelist=glob.glob('log_files/run*.log')

fl=sorted(filelist,cmp=run_compare,reverse=True)

fout=open(outfile,'w')

for file in fl:
    rname=file.split('/')[1].split('.')[0]
    firstline=rname+':'
    linenum=0;
    for line in open(file,'r'):
        if linenum == 0 and line[:len(firstline)] != firstline:
            fout.write(firstline + '\n\n')
        fout.write(line)
        linenum+=1

    fout.write("\n******************************************************\n\n")

fout.close()

subprocess.call('emacs -nw logs.log',shell=True)
