#!/usr/bin/env python

import sys;
import numpy;

if len(sys.argv) < 3:
    print "Proper usage: add_vmap_col.py $file_name $column_name";

linenum=0;

infile=open('versionmap.dat','r');
outfile=open('versionmap_backup.dat','w');
incol=open(sys.argv[1],'r');
outval='';

for line in infile:
    outfile.write(line);
    if linenum == 0:
        outval+=line.rstrip();
        outval+='\t'+sys.argv[2]+'\n';
    else:
        outval+=line.rstrip()+'\t'+incol.readline();
    linenum+=1;

outfile.close();
outfile=open('versionmap.dat','w');
outfile.write(outval);
