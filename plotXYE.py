#!/usr/bin/env python

import numpy
import pylab
import sys

if len(sys.argv) < 2:
	print 'Proper usage: plotXYE.py $infile [$xmin $xmax]'
	sys.exit()

data=numpy.loadtxt(sys.argv[1])
sz=numpy.shape(data)[0]
xind=range(sz)
if len(sys.argv) > 3:
	xmin=float(sys.argv[2])
	xmax=float(sys.argv[3])
	xind=[i for i in xrange(sz) if (data[i,0] >= xmin and data[i,0] <= xmax)]
pylab.errorbar(data[xind,0],data[xind,1],data[xind,2],fmt='.')
pylab.show()
