#!/usr/bin/env /global/home/users/mkolodru/bin/py

import scipy.sparse
import scipy.sparse.linalg
import sys
import numpy

num_gs=1
if len(sys.argv) > 1:
    num_gs=int(sys.argv[1])

basis_sz=int(open('H.out','r').readline())
Hvals=numpy.loadtxt('H.out',skiprows=1)
sz=Hvals.shape[0]
row_lst=numpy.zeros((sz),dtype=int)
col_lst=numpy.zeros((sz),dtype=int)
val_lst=numpy.zeros((sz),dtype=complex)
row_lst[:]=Hvals[:,0]
col_lst[:]=Hvals[:,1]
val_lst[:]=Hvals[:,2]+1.j*Hvals[:,3]
H=scipy.sparse.csr_matrix((val_lst,(row_lst,col_lst)),shape=(basis_sz,basis_sz))
try:
    (E_gs,gs)=scipy.sparse.linalg.eigsh(H,k=num_gs,which='SA')
except Exception as error:
    sys.stderr.write('Issues with convergence...\n')
    sys.exit()

numpy.savetxt('gs_real.out',numpy.real(gs))
numpy.savetxt('gs_imag.out',numpy.imag(gs))
numpy.savetxt('E_gs.out',E_gs)
