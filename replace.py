#! /usr/bin/env python

import sys
import glob
import string
import shutil

if len(sys.argv) < 3:
	print 'Proper usage: replace.py $init $final [$files]'
	sys.exit()

init=sys.argv[1]
fin=sys.argv[2]
if len(sys.argv) < 4:
	file_list=glob.glob("*"+init+"*")
	file_list.extend(glob.glob('*.dat'))
	file_list.extend(glob.glob('*.py'))
else:
	file_list=glob.glob(sys.argv[3])
	for j in xrange(4,len(sys.argv)):
		file_list.extend(glob.glob(sys.argv[j]))

for fname in file_list:
    try:
        f=open(fname,"r")
        val=f.read()
        f.close()
        f=open(fname,"w")
        f.write(string.replace(val, init, fin))
        shutil.move(fname, string.replace(fname, init, fin))
    except:
        pass
        
