#!/usr/bin/env py

import sys

if len(sys.argv)<3:
    print 'Usage: extract_real_imag.py $infile $realout $imagout'
    sys.exit()

realout=open(sys.argv[2],'w')
imagout=open(sys.argv[3],'w')

for line in open(sys.argv[1],'r'):
    pts=line.split()
    for pt in pts:
        realout.write(pt[1:-1].split(',')[0]+' ')
        imagout.write(pt[1:-1].split(',')[1]+' ')
    realout.write('\n')
    imagout.write('\n')
