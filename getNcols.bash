#!/usr/bin/env bash

for file in */*/N_*.out
do
    Nwfile=`echo "${file/N/Nw}"`
    Ncfile=`echo "${file/N/Nc}"`
    Nsfile=`echo "${file/N/Ns}"`
    Ndfile=`echo "${file/N/Nd}"`
    Nafile=`echo "${file/N/Na}"`
    Nnfile=`echo "${file/N/Nn}"`
    N0file=`echo "${file/N/N0}"`
    N1file=`echo "${file/N/N1}"`
    N2file=`echo "${file/N/N2}"`

    Nwcmd=`printf "extract_column.py %s %s 0" "$file" "$Nwfile"`
    Nscmd=`printf "extract_column.py %s %s 1" "$file" "$Nsfile"`
    Nccmd=`printf "extract_column.py %s %s 2" "$file" "$Ncfile"`
    Ndcmd=`printf "extract_column.py %s %s 3" "$file" "$Ndfile"`
    Nacmd=`printf "extract_column.py %s %s 4" "$file" "$Nafile"`
    Nncmd=`printf "extract_column.py %s %s 5" "$file" "$Nnfile"`
    N0cmd=`printf "extract_column.py %s %s 6" "$file" "$N0file"`
    N1cmd=`printf "extract_column.py %s %s 7" "$file" "$N1file"`
    N2cmd=`printf "extract_column.py %s %s 8" "$file" "$N2file"`

    echo $Nwcmd
    $Nwcmd
    echo $Nccmd
    $Nccmd
    echo $Nscmd
    $Nscmd
    echo $Ndcmd
    $Ndcmd
    echo $Nacmd
    $Nacmd
    echo $Nncmd
    $Nncmd
    echo $N0cmd
    $N0cmd
    echo $N1cmd
    $N1cmd
    echo $N2cmd
    $N2cmd
done