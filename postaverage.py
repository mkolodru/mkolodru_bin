#!/usr/bin/env python

import sys
import numpy

if len(sys.argv) < 3:
    print "Proper usage: postaverage.py $infile $AVCNT"
    assert 1==2


infile=open(sys.argv[1],'r')
AVCNT=int(sys.argv[2])
linenum=1
avg=0;
outvals=[]

try:
    for line in infile:
        avg+=float(line)
        if linenum % AVCNT==0:
            outvals.append(avg/AVCNT)
            avg=0
            linenum=0
        linenum+=1

    infile.close()
    outfile=open(sys.argv[1],'w')
    for val in outvals:
        outfile.write(str(val)+'\n')
except ValueError:
    print "Did not process file " + infile + " due to ValueError"
