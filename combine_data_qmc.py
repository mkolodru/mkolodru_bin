#!/usr/bin/env python

# Combine the data from multiple runs to a new "run"
# Assuming that data is stored in data.out file as in tfi_berry.cpp
# Usage: combine_data_qmc.py [$folder=./] [$run_file='runs.dat'] [$match_file='match.dat']

import numpy
import sys
import glob
import subprocess
import os

folder='./'
if len(sys.argv) > 1:
    folder=sys.argv[1]
if folder[-1]!='/':
    folder+='/'

cmd='cd '+folder+'; pwd > '+folder+'temp.temp; cd -'
print cmd
subprocess.call(cmd,shell=True)
current_run=open(folder+'temp.temp').readline()[:-1].split('/')[-1]
print 'current_run='+current_run
    
run_file=folder+'runs.dat'
match_file=folder+'match.dat'
if len(sys.argv) > 2:
    run_file=sys.argv[2]
if len(sys.argv) > 3:
    match_file=sys.argv[3]

# Read the list of runs to combine from run_file
# runs should be in format of folder name referenced to current directory
run_lst=[]
for line in open(run_file,'r'):
    run_lst.extend(line.split())
for j in xrange(len(run_lst)):
    if run_lst[j][-1]!='/':
        run_lst[j]+='/'

print 'run_lst='
print run_lst

# Read the list of rows in versionmap.dat that should be matched
match_str_lst=[]
for line in open(match_file,'r'):
    match_str_lst.extend(line.split())

print 'match_str_lst='
print match_str_lst

vmap_out=open(folder+'versionmap.dat','w')
vmap_out.write('vnum')
for match_str in match_str_lst:
    vmap_out.write('\t'+match_str)
vmap_out.write('\n')
vnum=0

used_lst=[]

j=0
while j < len(run_lst):
    run=run_lst[j]
    run_str=run[:-1].split('/')[-1]
    vmap_run=numpy.loadtxt(run+'versionmap.dat',skiprows=1)
    labels_run=open(run+'versionmap.dat').readline().split()
    vnum_run_col=labels_run.index('vnum')
    col_lst=[]
    for match_str in match_str_lst:
        col_lst.append(labels_run.index(match_str))

    for row in xrange(vmap_run.shape[0]):
        vnum_run=int(vmap_run[row,vnum_run_col])
        if tuple(vmap_run[row,col_lst]) not in used_lst:
            print 'About to check run='+run+', vnum='+str(vnum_run)
            print 'j='+str(j)+', row='+str(row)+' matches to...'
            
            used_lst.append(tuple(vmap_run[row,col_lst]))

            vmap_out.write(str(vnum))
            for col in col_lst:
                vmap_out.write('\t'+str(vmap_run[row,col]))
            vmap_out.write('\n')

            data_folder='/data/mkolodru/'+current_run+'/b'+str(vnum)+'/'
            if not os.path.exists(data_folder):
                os.makedirs(data_folder)
                
            data_out=open(data_folder+'data.out','w')
            for line in open('/data/mkolodru/'+run_str+'/b'+str(vnum_run)+'/data.out','r'):
                data_out.write(line)

            k=j+0
            while k < len(run_lst):
                run2=run_lst[k]
                run2_str=run2[:-1].split('/')[-1]
                vmap_run2=numpy.loadtxt(run2+'versionmap.dat',skiprows=1)
                labels_run2=open(run2+'versionmap.dat').readline().split()
                vnum_run2_col=labels_run2.index('vnum')
                col2_lst=[]
                for match_str in match_str_lst:
                    col2_lst.append(labels_run2.index(match_str))

                row2_start=0
                if j==k:
                    row2_start+=(row+1)
                    
                for row2 in xrange(row2_start,vmap_run2.shape[0]):
                    vnum_run2=int(vmap_run2[row2,vnum_run2_col])
                    if numpy.array_equal(vmap_run2[row2,col2_lst],vmap_run[row,col_lst]):
                        print '\tk='+str(k)+', row2='+str(row2)
                        first=True
                        for line in open('/data/mkolodru/'+run2_str+'/b'+str(vnum_run2)+'/data.out','r'):
                            if first:
                                first=False
                            else:
                                data_out.write(line)

                k+=1

            vnum+=1

    j+=1
    
#Log the current run
logstr=current_run+':\n\n'
logstr+='Fake run combining the data in runs '+run_lst[0]
for j in xrange(1,len(run_lst)):
    logstr+=', '+run_lst[j]
logstr+='\n\nCluster: buphyg'
open(current_run+'.log','w').write(logstr)

