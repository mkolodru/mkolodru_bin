#!/usr/bin/env python

import subprocess
import sys
import os
import glob

if len(sys.argv) < 1:
	print 'Proper usage: backup_data.py [$folder]'
	sys.exit()

rel_folder=''
if len(sys.argv) > 1:
	rel_folder=sys.argv[1]
if len(rel_folder)>0 and rel_folder[-1]=='/':
	rel_folder=rel_folder[:-1]

curr_folder=os.getcwd()

if len(rel_folder)==0:
	folder=curr_folder
elif rel_folder[0]!='/':
	folder=curr_folder+'/'+rel_folder
else:
	folder=rel_folder

pts=folder.split('/')
assert len(pts) >= 4
run_name=pts[-1]
assert pts[-2]=='build'
sim_name=pts[-3]
assert pts[-4]=='mkolodru'

#First remove any already backed-up data
previous_data_files=folder+'/R*/'
cmd='rm -r '+previous_data_files
print cmd
subprocess.call(cmd,shell=True)

#Second create a tar file of all the data
tar_postfix=run_name+'_data.tgz'
tar_file=folder+'/'+tar_postfix
data_files=folder+'/b*/'

if len(glob.glob(data_files))==0:
	print 'There is no data to transfer'
	sys.exit()

cmd='cd '+folder+'; tar -czf '+tar_postfix+' b*/; cd -'
print cmd
subprocess.call(cmd,shell=True)

#Then scp the tar file to the external h.d.
ext_hd_dir='mkolodru-cmt:~/backup/'
ext_folder=ext_hd_dir+sim_name+'/build/'
cmd='scp '+tar_file+' '+ext_folder
print cmd
subprocess.call(cmd,shell=True)

#Then rm the tar file, the raw data, and the previously backed-up data
cmd='rm '+tar_file+'; rm -r '+data_files
print cmd
subprocess.call(cmd,shell=True)
